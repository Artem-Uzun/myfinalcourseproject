﻿using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    /// <summary>
    /// implements ICardsRepository
    /// </summary>
    public class CardRepository : ICardRepository
    {
        private readonly CardFileDbContext db;
        public CardRepository(CardFileDbContext context)
        {
            this.db = context;
        }

        public async Task AddAsync(Card entity)
        {
            await db.Cards.AddAsync(entity);
        }

        public void Delete(Card entity)
        {
            db.Cards.Remove(entity);
        }

        public void DeleteById(int id)
        {
            var toRemove = db.Cards.Find(id);
            db.Cards.Remove(toRemove);
        }
        public IQueryable<Card> FindAll()
        {
            db.CardTags.Load();
            db.Tags.Load();
            return db.Cards.Include(x => x.Sphere).Include(x => x.Status).Include(x => x.User);
        }
        public IQueryable<Card> FindAllByUserId(int userId)
        {
            return db.Cards.Where(x => x.UserId == userId);
        }
        public async Task<Card> GetByIdAsync(int id)
        {
            db.Spheres.Load();
            db.Statuses.Load();
            db.Tags.Load();
            db.CardTags.Load();
            db.Users.Load();
            return await db.Cards.FindAsync(id);
        }
        public void Update(Card entity)
        {
            db.Spheres.Load();
            db.Statuses.Load();
            db.Tags.Load();
            var card = db.Cards.First(x => x.Id == entity.Id);
            if (card != null)
            {
                card.Publicity = entity.Publicity;
                card.Sphere = entity.Sphere;
                card.Tags = entity.Tags;
                card.Text = entity.Text;
            }
        }
    }
}
