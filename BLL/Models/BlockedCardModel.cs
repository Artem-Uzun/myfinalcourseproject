﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    /// <summary>
    /// Represents the model of card to block/unblock
    /// </summary>
    public class BlockedCardModel
    {
        public int Id { get; set; }
        public string AdminName { get; set; }
        public string Reason { get; set; }
    }
}
