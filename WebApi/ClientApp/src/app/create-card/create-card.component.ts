import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DbInfoService } from '../db-info.service';
import { CardModel } from '../types';

@Component({
  selector: 'app-create-card',
  templateUrl: './create-card.component.html',
  styleUrls: ['./create-card.component.css']
})
export class CreateCardComponent implements OnInit {
  card: CardModel;
  form: FormGroup;
  baseUrl: string;
  errorResponse: string;
  creating: boolean;
  disableButton: boolean;

  constructor(private formBuilder: FormBuilder, private router: Router,
              private http: HttpClient, @Inject('BASE_URL') baseUrl: string,
              private dbInfoService: DbInfoService) { 
    this.creating = true;
    if (this.router.getCurrentNavigation().extras.state){
      this.card = this.router.getCurrentNavigation().extras.state.model;
      this.creating = false;
    }
    this.baseUrl = baseUrl;
  }

  ngOnInit() {
    this.disableButton = false;
    let cardExist = this.card != undefined;
    this.form = this.formBuilder.group({
      privacy: cardExist&&(!this.card.publicity),
      sphereName: cardExist ? this.card.sphereName : '',
      text: cardExist ? this.card.text : '',
      tagsNames: cardExist ? this.card.tagsNames : '',
    });
  }
  submitCreate(): void {
    this.disableButton = true;
    let values = this.form.getRawValue();
    this.card = {
      id: undefined,
      userName: undefined,
      published: undefined,
      publicity: !values.privacy,
      tagsNames: values.tagsNames != '' ? values.tagsNames : [],
      sphereName: values.sphereName != '' ? values.sphereName : undefined,
      text: values.text,
      active: undefined,
      adminName: undefined,
      statusText: undefined,
    };
    this.http.post<CardModel>(this.baseUrl + 'api/cards', this.card)
    .subscribe(result => {
      this.router.navigate(['cards']);
    }, error => {
      this.errorResponse = error.error; 
      this.disableButton = false;
    });
  }
  submitEdit(): void {
    this.disableButton = true;
    let values = this.form.getRawValue();
    this.card.publicity = !values.privacy;
    this.card.sphereName = values.sphereName != '' ? values.sphereName : undefined;
    this.card.tagsNames = values.tagsNames != '' ? values.tagsNames : []
    this.card.text = values.text;
    this.http.put<CardModel>(this.baseUrl + 'api/cards/' + this.card.id, this.card)
    .subscribe(result => {
     this.router.navigate(['my']);
    }, error => {
      this.errorResponse = error.error;
      this.disableButton = false;
    });
  }
}
