import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ResponseModel } from '../types';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  form: FormGroup;
  baseUrl:string;
  response: ResponseModel;
  disableButton: boolean;
  formErrors: string[];

  constructor(private formBuilder:FormBuilder, private http: HttpClient,
    @Inject('BASE_URL') baseUrl: string, private router: Router) { 
      this.baseUrl = baseUrl;
    }

  ngOnInit() {
    this.disableButton = false;
    this.form = this.formBuilder.group({
      username: '',
      password: '',
      rules: false
    });
  }
  submit(): void {
    let values = this.form.getRawValue();
    if (values.rules == false){
      this.response = {
        message:'must accept the rules',
        ...values
      };
      return;
    }
    this.disableButton = true;
    this.http.post<ResponseModel>(this.baseUrl + 'api/account/registration', this.form.getRawValue())
    .subscribe(result => {
      this.response = result;
      this.response.message = 'Successfully logined';
      this.router.navigate(['/login']);
      setTimeout(()=>{ window.location.reload() }, 1000)
    }, error => {
      this.response = error.error;
      this.formErrors = error.error.errors;
      this.disableButton = false;
    });
  }
}
