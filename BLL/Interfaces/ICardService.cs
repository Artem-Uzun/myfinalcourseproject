﻿using Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    /// <summary>
    /// Represents the service class to work with cards
    /// </summary>
    public interface ICardService
    {
        /// <summary>
        /// Get card model by SearchModel filter
        /// </summary>
        /// <param name="filterSearch">Object to filter data</param>
        /// <returns>CardModels that fit the filter</returns>
        IEnumerable<CardModel> GetByFilter(SearchModel filterSearch);
        /// <summary>
        /// Checks if card belongs to the user
        /// </summary>
        /// <param name="cardId">id of the card</param>
        /// <param name="username">username of the user</param>
        /// <returns>true if card belongs to the user, otherwise false</returns>
        Task<bool> CardOfUserAsync(int cardId, string username);
        /// <summary>
        /// Get card by id if it is active
        /// </summary>
        /// <param name="id">id of the card</param>
        /// <returns>full card if card is active, id and status if card is blocked</returns>
        Task<CardModel> GetActiveByIdAsync(int id);
        /// <summary>
        /// get spheres and tags of db
        /// </summary>
        /// <returns>DbInfoModel with spheres and tags of db</returns>
        DbInfoModel GetDbInfo();
        /// <summary>
        /// Set card status as blocked
        /// </summary>
        /// <param name="model">model contains cardId, admin name and reason why card was banned</param>
        Task BanCard(BlockedCardModel model);
        /// <summary>
        /// Set card status as active
        /// </summary>
        /// <param name="cardId">id of the card</param>
        Task UnBanCard(int cardId);
        /// <summary>
        /// get card by id
        /// </summary>
        /// <param name="id">card id</param>
        /// <returns>CardModel of the card</returns>
        Task<CardModel> GetByIdAsync(int id);
        /// <summary>
        /// add card to db
        /// </summary>
        /// <param name="model">card model to add</param>
        /// <exception cref="NullReferenceException">if there is no card with id</exception>
        /// <returns>Card model</returns>
        Task AddAsync(CardModel model);
        /// <summary>
        /// Update Card
        /// </summary>
        /// <param name="model">card to update</param>
        Task UpdateAsync(CardModel model);
        /// <summary>
        /// delete card by id
        /// </summary>
        /// <param name="modelId">card id</param>
        Task DeleteByIdAsync(int modelId);
    }
}
