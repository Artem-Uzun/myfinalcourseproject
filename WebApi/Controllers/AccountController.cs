﻿using Business;
using Data.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Business.Models;
using Business.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private IUserService Service { get; set; }
        //Inject book service via constructor
        public AccountController(IUserService s)
        {
            Service = s;
        }
        [HttpPost("registration")]
        public async Task<IActionResult> RegisterAsync([FromBody]UserModel model)
        {
            if (User.IsInRole("user") || User.IsInRole("admin"))
            {
                ResponseModel response = new ResponseModel
                {
                    Message = "You are already registered",
                    IsSuccess = false
                };
                return BadRequest(response);
            }
            if (ModelState.IsValid)
            {
                var result = await Service.RegisterUserAsync(model);
                if (result.IsSuccess)
                {
                    return Ok(result);
                }
                return BadRequest(result);
            }
            return BadRequest(new ResponseModel
            {
                Message = "Some properties are not valid",
                IsSuccess = false
            });
            
        }
        [HttpPost("login")]
        public async Task<IActionResult> LogInAsync([FromBody] UserModel model)
        {
            if (User.IsInRole("user"))
            {
                ResponseModel response = new ResponseModel
                {
                    Message = "You already signed in",
                    IsSuccess = false
                };
                return BadRequest(response);
            }
            if (ModelState.IsValid)
            {
                var result = await Service.LogInUserAsync(model);
                if (result.IsSuccess)
                {
                    return Ok(result);
                }
                return BadRequest(result);
            }
            return BadRequest(new ResponseModel
            {
                Message = "Invalid input",
                IsSuccess = false
            });
        }
        ////https://localhost:44318/token, каким то образом без /api/account
        //[HttpPost("/token")]
        //public async Task<IActionResult> Token([FromBody] UserModel userModel)
        //{
        //    var identity = await GetIdentityAsync(userModel.UserName, userModel.Password);
        //    if (identity == null)
        //    {
        //        return BadRequest("Invalid username or password");
        //    }
        //    var now = DateTime.UtcNow;
        //    var jwt = new JwtSecurityToken(
        //        issuer: AuthOptions.ISSUER,
        //        audience: AuthOptions.AUDIENCE,
        //        notBefore: now,
        //        claims: identity.Claims,
        //        expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
        //        signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(),
        //        SecurityAlgorithms.HmacSha256));
        //    string encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
        //    var response = new
        //    {
        //        access_token = encodedJwt,
        //        username = identity.Name
        //    };
        //    return Json(response);
        //}
        //private async Task<ClaimsIdentity> GetIdentityAsync(string username, string password)
        //{

        //    var signIn = await Unit.SignInManager.PasswordSignInAsync(username, password, false, false);
        //    if (signIn.Succeeded)
        //    {
        //        var user = await Unit.UserManager.FindByNameAsync(username);
        //        var roles = await Unit.UserManager.GetRolesAsync(user);
        //        var claims = new List<Claim>
        //        {
        //            new Claim(ClaimsIdentity.DefaultNameClaimType, user.UserName),
        //            new Claim(ClaimsIdentity.DefaultRoleClaimType, roles.First())
        //        };
        //        ClaimsIdentity claimsIdentity =
        //            new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
        //            ClaimsIdentity.DefaultRoleClaimType);
        //        return claimsIdentity;
        //    }
        //    //if user not found
        //    return null;
        //}
    }
}
