﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    /// <summary>
    /// Contains all tags and sphere names from database
    /// </summary>
    public class DbInfoModel
    {
        public ICollection<string> Tags { get; set; }
        public ICollection<string> Spheres { get; set; }
    }
}
