import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DbInfoService } from '../db-info.service';
import { CardModel } from '../types';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {
  public response: CardModel[];
  public errorResponse: string;
  form: FormGroup;
  disableButton: boolean;
  baseUrl: string;
  role:string;

  constructor(private formBuilder:FormBuilder, private http: HttpClient, 
            @Inject('BASE_URL') baseUrl: string, private dbInfoService: DbInfoService) { 
    this.baseUrl = baseUrl;
    http.post<CardModel[]>(baseUrl + 'api/cards/filter',{}).subscribe(result => {
      this.response = result;
    }, error =>{
       this.errorResponse = error.error;
       console.log(error.error);
    });
  }

  ngOnInit() {
    this.role = localStorage.getItem('role');
    this.disableButton = false;
    this.form = this.formBuilder.group({
      userName: '',
      privacy: false,
      dateFrom: "2021-05-01",
      dateTo: "2021-06-25",
      sphereName: '',
      tagsNames: '',
    });
  }
  search() {
    this.disableButton = true;
    let values = this.form.getRawValue();
    let searchForm = {
      userName: values.userName != '' ? values.userName : undefined,
      publishedAfter: values.dateFrom,
      publishedBefore: values.dateTo,
      publicity: !values.privacy,
      sphereName: values.sphereName != '' ? values.sphereName : undefined,
      tags: values.tagsNames != '' && values.tagsNames[0] != '' ? values.tagsNames : undefined,
    }
    this.http.post<CardModel[]>(this.baseUrl + 'api/cards/filter', searchForm).subscribe(result => {
      this.response = result;
      this.disableButton = false;
    }, error =>{
      this.errorResponse = error.error;
      this.disableButton = false;
    });
  }
}