﻿using AutoMapper;
using Business.Models;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<BlockedCardModel, CardModel>()
                .ForMember(p => p.StatusText, c => c.MapFrom(card => card.Reason));
            CreateMap<Card, BlockedCardModel>()
                .ForMember(p=>p.AdminName, c=>c.MapFrom(card=>card.Status.AdminBlocked.UserName))
                .ForMember(p=>p.Reason, c=>c.MapFrom(card=>card.Status.Text));
            CreateMap<Card, CardModel>()
                .ForMember(p => p.SphereName, c => c.MapFrom(card => card.Sphere != null ? card.Sphere.Name : null))
                .ForMember(p => p.UserName, c => c.MapFrom(card => card.User.UserName))
                .ForMember(p=>p.Active, c=>c.MapFrom(card=>card.Status.Active))
                .ForMember(p=>p.AdminName, c=>c.MapFrom(card=>card.Status.AdminBlocked.UserName))
                .ForMember(p => p.TagsNames, c => c.MapFrom(card => card.Tags != null ? card.Tags.Select(x => x.Tag.TagName) : null));
            CreateMap<CardModel, Card>()
                .ForMember(p => p.Sphere, c=>c.Ignore())
                .ForMember(p => p.Tags, c => c.Ignore());
            CreateMap<CardModel, Status>()
                .ForMember(p => p.Text, c => c.MapFrom(card => card.StatusText));
        }
    }
}
