import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { CardModel } from '../types';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.css']
})
export class UserCardComponent implements OnInit {
  public response: CardModel[];
  public errorResponse: string;
  role:string;
  
  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<CardModel[]>(baseUrl + 'api/cards/user').subscribe(result => {
      this.response = result;
    }, error =>{
       this.errorResponse = error.error;
    });
   }

  ngOnInit() {
    this.role = localStorage.getItem('role');
  }

}
