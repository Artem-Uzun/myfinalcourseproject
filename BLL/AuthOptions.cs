﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business
{
    /// <summary>
    /// class to create the JWT token
    /// </summary>
    public class AuthOptions
    {
        //token publisher
        public const string ISSUER  = "MyFinalCourseProject_JWT_demoServer";

        //token user
        public const string AUDIENCE = "MyFinalCourseProject_JWT_demoClient";

        //key for encripting
        const string KEY = "mysupersecret_secretkey!123";

        //token ttl - 60 minutes
        public const int LIFETIME = 60;
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
