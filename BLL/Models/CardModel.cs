﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Business.Models
{
    /// <summary>
    /// Represents the model of card
    /// </summary>
    public class CardModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public DateTime Published { get; set; }
        public bool Publicity { get; set; }
        public ICollection<string> TagsNames { get; set; }
        public string SphereName { get; set; }
        public string Text { get; set; }
        [JsonIgnore]
        public int StatusId { get; set; }
        public bool Active { get; set; }
        public string AdminName { get; set; }
        public string StatusText { get; set; }
    }
}
