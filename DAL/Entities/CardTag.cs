﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    /// <summary>
    /// Represents the many to many relation between tag and card
    /// </summary>
    public class CardTag : BaseEntity
    {
        public int CardId { get; set; }
        public Card Card { get; set; }
        public int TagId { get; set; }
        public Tag Tag { get; set; }
    }
}
