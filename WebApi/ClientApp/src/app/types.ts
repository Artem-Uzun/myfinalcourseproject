export interface UserModel {
    userName: string;
    password: string;
}
export interface ResponseModel {
    message: string;
    isSuccess: boolean;
    errors: string[];
    userName: string;
    userRole: string;
}
export interface SearchModel {
    userName: string;
    publishedAfter: Date;
    publishedBefore: Date;
    publicity: boolean;
    sphereName: string;
    tags: string[];
}
export interface BlockedCardModel{
    id: number;
    reason: string;
}
export interface CardModel {
    id: number;
    userName: string;
    published: Date;
    publicity: boolean;
    tagsNames: string[];
    sphereName: string;
    text: string;
    active: boolean;
    adminName: string;
    statusText: string;
}
export interface DbInfoModel {
    tags: string[];
    spheres: string[];
}