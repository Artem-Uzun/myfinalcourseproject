﻿using Data.Interfaces;
using Data.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    /// <summary>
    /// implements IUnitOfWork
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CardFileDbContext db;
        private CardRepository cardRepository;
        private SphereRepository sphereRepository;
        private StatusRepository statusRepository;
        private TagRepository tagRepository;
        private UserManager<IdentityUser> userManager;
        private RoleManager<IdentityRole> roleManager;
        private SignInManager<IdentityUser> signInManager;
        public UnitOfWork(CardFileDbContext c, UserManager<IdentityUser> u, RoleManager<IdentityRole> r, SignInManager<IdentityUser> s)
        {
            userManager = u;
            db = c;
            roleManager = r;
            signInManager = s;
        }
        public UserManager<IdentityUser> UserManager
        {
            get
            {
                return userManager;
            }
        }
        public RoleManager<IdentityRole> RoleManager
        {
            get
            {
                return roleManager;
            }
        }
        public SignInManager<IdentityUser> SignInManager
        {
            get
            {
                return signInManager;
            }
        }
        public ICardRepository CardRepository
        {
            get
            {
                if (cardRepository == null)
                    cardRepository = new CardRepository(db);
                return cardRepository;
            }
        }

        public ISphereRepository SphereRepository
        {
            get
            {
                if (sphereRepository == null)
                    sphereRepository = new SphereRepository(db);
                return sphereRepository;
            }
        }

        public IStatusRepository StatusRepository
        {
            get
            {
                if (statusRepository == null)
                    statusRepository = new StatusRepository(db);
                return statusRepository;
            }
        }
        public ITagRepository TagRepository
        {
            get
            {
                if (tagRepository == null)
                    tagRepository = new TagRepository(db);
                return tagRepository;
            }
        }

        public Task<int> SaveAsync()
        {
            return db.SaveChangesAsync();
        }
        public IDbContextTransaction GetTransaction()
        {
            return db.Database.BeginTransaction();
        }
    }
}
