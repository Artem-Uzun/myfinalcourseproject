﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    /// <summary>
    /// Represents the unit of work interface of unit of work pattern.
    /// Contains repositories and managers to work with db
    /// </summary>
    public interface IUnitOfWork
    {
        ICardRepository CardRepository { get; }
        ISphereRepository SphereRepository { get; }
        IStatusRepository StatusRepository { get; }
        ITagRepository TagRepository { get; }
        UserManager<IdentityUser> UserManager { get; }
        SignInManager<IdentityUser> SignInManager { get; }
        RoleManager<IdentityRole> RoleManager { get; }
        IDbContextTransaction GetTransaction();
        Task<int> SaveAsync();
    }
}
