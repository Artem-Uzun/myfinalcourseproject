﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    /// <summary>
    /// Contains id for entities
    /// </summary>
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
