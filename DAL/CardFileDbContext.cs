﻿using Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    /// <summary>
    /// Represents the db context class
    /// </summary>
    public class CardFileDbContext : IdentityDbContext<IdentityUser>
    {
        public CardFileDbContext(DbContextOptions<CardFileDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
        public DbSet<Card> Cards { get; set; }
        public DbSet<Sphere> Spheres { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<CardTag> CardTags { get; set; }
    }
}
