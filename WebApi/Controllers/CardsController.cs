﻿using Business.Interfaces;
using Business.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class CardsController : ControllerBase
    {
        private ICardService Service { get; set; }
        //Inject book service via constructor
        public CardsController(ICardService s)
        {
            Service = s;
        }
        /// <summary>
        /// Returns the card by card id
        /// </summary>
        /// <param name="id">Card id</param>
        /// <returns>Card</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<CardModel>>> GetById(int id)
        {
            try
            {
                if (User.IsInRole("admin"))
                {
                    return Ok(await Service.GetByIdAsync(id));
                }
                return Ok(await Service.GetActiveByIdAsync(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Returns tags and spheres names from db
        /// </summary>
        /// <returns>DbInfoModel with all tags and spheres names</returns>
        [HttpGet("/dbinfo")]
        public ActionResult<DbInfoModel> GetDbInfo()
        {
            try
            {
                var t = Service.GetDbInfo();
                return Ok(t);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Returns cards that fit the filter
        /// </summary>
        /// <param name="model">filter model</param>
        /// <returns>cards</returns>
        [HttpPost("filter")]
        public ActionResult<IEnumerable<CardModel>> GetByFilter([FromBody] SearchModel model)
        {
            try
            {
                if (!User.IsInRole("admin"))
                {
                    model.Publicity = true;
                   
                }
                else
                {
                    model.Active = false;
                }
                return Ok(Service.GetByFilter(model));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Returns cards of current user
        /// </summary>
        /// <returns>cards</returns>
        [HttpGet("user")]
        [Authorize]
        public ActionResult<IEnumerable<CardModel>> GetByUser()
        {
            //admin should get banned also
            try
            {
                SearchModel model = new SearchModel
                {
                    Publicity = null,
                    UserName = User.Identity.Name,
                };
                if (User.IsInRole("admin"))
                {
                    model.Active = false;
                }
                return Ok(Service.GetByFilter(model));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Add card to database
        /// </summary>
        /// <param name="model">card to add</param>
        [HttpPost]
        [Authorize]
        public async Task<ActionResult> Add([FromBody] CardModel model)
        {
            try
            {
                model.UserName = User.Identity.Name;
                model.Published = DateTime.Now;
                await Service.AddAsync(model);
                return Ok(model);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Update card in database
        /// </summary>
        /// <param name="model">new data of card</param>
        /// <param name="id">card id</param>
        [HttpPut("{id}")]
        [Authorize]
        public async Task<ActionResult> Update([FromBody] CardModel model, int id)
        {
            if (!(await Service.CardOfUserAsync(id, User.Identity.Name)))
            {
                return BadRequest("can't update card of another user");
            }
            try
            {
                model.Id = id;
                await Service.UpdateAsync(model);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Blocks card
        /// </summary>
        /// <param name="model">block model</param>
        [HttpPost("ban")]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Ban([FromBody] BlockedCardModel model)
        {
            try
            {
                model.AdminName = User.Identity.Name;
                await Service.BanCard(model);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Unblock card by id
        /// </summary>
        /// <param name="cardId">card id</param>
        [HttpPost("unban")]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> UnBan([FromBody] int cardId)
        {
            try
            {
                await Service.UnBanCard(cardId);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Delete card from database
        /// </summary>
        /// <param name="cardId">card id</param>
        [HttpDelete("{cardId}")]
        [Authorize]
        public async Task<ActionResult> Delete(int cardId)
        {
            if (!(await Service.CardOfUserAsync(cardId, User.Identity.Name)))
            {
                return BadRequest("can't delete card of another user");
            }
            try
            {
                await Service.DeleteByIdAsync(cardId);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
