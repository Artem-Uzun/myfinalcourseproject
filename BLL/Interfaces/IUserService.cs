﻿using Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    /// <summary>
    /// Represents the service class to work with users
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// Create new user in db
        /// </summary>
        /// <param name="model">user model</param>
        /// <returns>response with information</returns>
        Task<ResponseModel> RegisterUserAsync(UserModel model);
        /// <summary>
        /// login user
        /// </summary>
        /// <param name="model">user model</param>
        /// <remarks>creates jwt token and add it to response if login successfully</remarks>
        /// <returns>response with information</returns>
        Task<ResponseModel> LogInUserAsync(UserModel model);
    }
}
