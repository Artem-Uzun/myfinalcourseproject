﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    /// <summary>
    /// Represents the interface of repository to work with sphere entity
    /// </summary>
    public interface ISphereRepository : IRepository<Sphere>
    {
        Sphere FindByName(string name);
    }
}
