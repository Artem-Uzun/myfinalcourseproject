﻿using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Data.Entities;
using Data.Interfaces;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class CardService : ICardService
    {
        private IUnitOfWork Unit { get; set; }
        private Mapper _mapper { get; set; }
        public CardService(IUnitOfWork unit, Mapper mapper)
        {
            this.Unit = unit;
            this._mapper = mapper;
        }
        public async Task AddAsync(CardModel model)
        {
            checkModel(model);
            var transaction = Unit.GetTransaction();
            try
            {
                //var cardStatus = _mapper.Map<CardModel, Status>(model);
                var cardStatus = new Status
                {
                    Id = model.Id,
                    Active = true,
                };
                await Unit.StatusRepository.AddAsync(cardStatus);
                var t = _mapper.Map<CardModel, Card>(model);
                t.Status = cardStatus;
                if (model.SphereName != null)
                {
                    t.Sphere = Unit.SphereRepository.FindByName(model.SphereName);
                }
                t.User = await Unit.UserManager.FindByNameAsync(model.UserName);
                await Unit.CardRepository.AddAsync(t);
                if (model.TagsNames != null)
                {
                    foreach (var tagName in model.TagsNames)
                    {
                        var tag = new CardTag
                        {
                            Card = t,
                            Tag = Unit.TagRepository.FindByName(tagName)
                        };
                        await Unit.TagRepository.AddCardTagAsync(tag);
                    }
                }
                await Unit.SaveAsync();
                await transaction.CommitAsync();
            }
            catch
            {
                await transaction.RollbackAsync();
                throw;
            }
        }
        public async Task<bool> CardOfUserAsync(int modelId, string username)
        {
            var currentUser = await Unit.UserManager.FindByNameAsync(username);
            if (currentUser != (await Unit.CardRepository.GetByIdAsync(modelId)).User)
            {
                return false;
            }
            return true;
        }
        public async Task DeleteByIdAsync(int modelId)
        {
            var transaction = Unit.GetTransaction();
            try
            {
                var t = await Unit.CardRepository.GetByIdAsync(modelId);
                Unit.StatusRepository.DeleteById(t.Status.Id);
                Unit.CardRepository.DeleteById(t.Id);
                await Unit.SaveAsync();
                await transaction.CommitAsync();
            }
            catch
            {
                await transaction.RollbackAsync();
                throw;
            }
        }
        public async Task BanCard(BlockedCardModel model)
        {
            var transaction = Unit.GetTransaction();
            try
            {
                var cardStatus = (await Unit.CardRepository.GetByIdAsync(model.Id)).Status;
                var newStatus = new Status
                {
                    Id = cardStatus.Id,
                    Active = false,
                    AdminBlocked = await Unit.UserManager.FindByNameAsync(model.AdminName),
                    Text = model.Reason
                };
                Unit.StatusRepository.Update(newStatus);
                await Unit.SaveAsync();
                await transaction.CommitAsync();
            }
            catch
            {
                await transaction.RollbackAsync();
                throw;
            }
        }
        public async Task UnBanCard(int cardId)
        {
            var transaction = Unit.GetTransaction();
            try
            {
                var cardStatus = (await Unit.CardRepository.GetByIdAsync(cardId)).Status;
                var newStatus = new Status
                {
                    Id = cardStatus.Id,
                    Active = true,
                };
                Unit.StatusRepository.Update(newStatus);
                await Unit.SaveAsync();
                await transaction.CommitAsync();
            }
            catch
            {
                await transaction.RollbackAsync();
                throw;
            }
        }
        public DbInfoModel GetDbInfo()
        {
            var t = new DbInfoModel
            {
                Tags = Unit.TagRepository.FindAll().Select(x => x.TagName).ToList(),
                Spheres = Unit.SphereRepository.FindAll().Select(x => x.Name).ToList()
            };
            return t;
        }
        public IEnumerable<CardModel> GetByFilter(SearchModel filterSearch)
        {
            List<Card> cards = new List<Card>();
            foreach (var card in Unit.CardRepository.FindAll())
            {
                var cardTags = card.Tags?.Select(x => x.Tag.TagName);
                if ((filterSearch.UserName == null || filterSearch.UserName == card.User.UserName) 
                    && (filterSearch.Publicity == null || filterSearch.Publicity == card.Publicity) 
                    && (filterSearch.PublishedAfter == default || filterSearch.PublishedAfter <= card.Published)
                    && (filterSearch.PublishedBefore == default || filterSearch.PublishedBefore >= card.Published)
                    && (filterSearch.SphereName == null || filterSearch.SphereName == card.Sphere?.Name)
                    && (filterSearch.Active == false || card.Status.Active == true)
                    && (filterSearch.Tags == null || (cardTags != null && !filterSearch.Tags.Except(cardTags).Any()))
                    )
                {
                    cards.Add(card);
                }
            }
            return _mapper.Map<IEnumerable<CardModel>>(cards);
        }
        public async Task<CardModel> GetByIdAsync(int id)
        {

            var model = await Unit.CardRepository.GetByIdAsync(id);
            if (model == null)
            {
                throw new NullReferenceException("Card doesn't exist");
            }
            return _mapper.Map<CardModel>(model);
        }
        public async Task<CardModel> GetActiveByIdAsync(int id)
        {
            var model = await Unit.CardRepository.GetByIdAsync(id);
            if (model == null)
            {
                throw new ArgumentNullException("Card doesn't exist");
            }
            if (!model.Status.Active)
            {
                var blockedModel = _mapper.Map<BlockedCardModel>(model);
                return _mapper.Map<CardModel>(blockedModel);
            }
            return _mapper.Map<CardModel>(model);
        }
        public async Task UpdateAsync(CardModel model)
        {
            var updatingCard = await Unit.CardRepository.GetByIdAsync(model.Id);
            if (!updatingCard.Status.Active)
            {
                throw new InvalidOperationException("Banned card can not be updated");
            }
            model.Published = updatingCard.Published;
            checkModel(model);
            var transaction = Unit.GetTransaction();
            try
            {
                var card = _mapper.Map<CardModel, Card>(model);
                var status = _mapper.Map<CardModel, Status>(model);
                List<int> currentTags = Unit.TagRepository.GetCardTags().Where(x => x.CardId == model.Id).Select(x => x.TagId).ToList();
                Unit.StatusRepository.Update(status);
                if (model.SphereName != null)
                {
                    card.Sphere = Unit.SphereRepository.FindByName(model.SphereName);
                }
                card.User = await Unit.UserManager.FindByNameAsync(model.UserName);
                Unit.CardRepository.Update(card);
                List<int> newTags = new List<int>();
                foreach (var tagName in model.TagsNames)
                {
                    newTags.Add(Unit.TagRepository.FindByName(tagName).Id);
                }
                foreach (var tagIdToDelete in currentTags.Except(newTags))
                {
                    Unit.TagRepository.DeleteCardTag(model.Id, tagIdToDelete);
                }
                foreach (var tagIdToAdd in newTags.Except(currentTags))
                {
                    await Unit.TagRepository.AddCardTagAsync(new CardTag
                    {
                        CardId = model.Id,
                        TagId = tagIdToAdd
                    });
                }
                await Unit.SaveAsync();
                await transaction.CommitAsync();
            }
            catch
            {
                await transaction.RollbackAsync();
                throw;
            }
        }
        private void checkModel(CardModel model)
        {
            if (model.Published == default)
            {
                throw new ArgumentNullException();
            }
            if (model.Text == default)
            {
                throw new ArgumentNullException();
            }
        }
    }
}
