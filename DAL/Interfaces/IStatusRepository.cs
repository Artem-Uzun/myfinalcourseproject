﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Interfaces
{
    /// <summary>
    /// Represents the interface of repository to work with status entity
    /// </summary>
    public interface IStatusRepository : IRepository<Status>
    {
    }
}
