import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, OnInit } from '@angular/core';
import { DbInfoModel } from './types';

@Injectable({
  providedIn: 'root'
})
export class DbInfoService {
  baseUrl: string;
  public tags: string[];
  public spheres: string[];
  public operationEnds: boolean;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) { 
    this.baseUrl = baseUrl;
    this.getData();
  }

  private getData() {
    this.operationEnds = false;
    this.http.get<DbInfoModel>(this.baseUrl + 'dbinfo').subscribe(result => {
      this.tags = result.tags;
      this.spheres = result.spheres;
    }, error =>{
    });
    this.operationEnds = true;
  }
}
