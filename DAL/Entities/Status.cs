﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    /// <summary>
    /// Represents the card status
    /// </summary>
    /// <param name="Active">Active if true, blocked if false</param>
    /// <param name="AdminBlocked">Username of the admin who blocked the card</param>
    /// <param name="Text">Reason why the card was blocked</param>
    public class Status : BaseEntity
    {
        public bool Active { get; set; } = true;
        public IdentityUser AdminBlocked { get; set; }
        public string Text { get; set; }
    }
}
