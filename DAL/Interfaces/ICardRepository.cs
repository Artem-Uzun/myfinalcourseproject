﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    /// <summary>
    /// Represents the interface of repository to work with card entity
    /// </summary>
    public interface ICardRepository : IRepository<Card>
    {
        IQueryable<Card> FindAllByUserId(int userId);
    }
}
