﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    /// <summary>
    /// Represents the interface of repository to work with tag entity
    /// </summary>
    public interface ITagRepository : IRepository<Tag>
    {
        Tag FindByName(string name);
        Task AddCardTagAsync(CardTag entity);
        void DeleteCardTag(int cardId, int tagId);
        IEnumerable<CardTag> GetCardTags();
    }
}
