﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    /// <summary>
    /// Represents the Tag for specifying card info
    /// </summary>
    /// <param name="TagName">Name of the tag</param>
    /// <param name="Cards">Cards with the tag</param>
    public class Tag : BaseEntity
    {
        public string TagName { get; set; }
        public ICollection<CardTag> Cards { get; set; }
    }
}
