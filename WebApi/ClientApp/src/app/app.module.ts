import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { CardsComponent } from './cards/cards.component';
import { InterceptorService } from './interceptor.service';
import { LoginComponent } from './login/login.component';
import { SingleCardComponent } from './single-card/single-card.component';
import { CreateCardComponent } from './create-card/create-card.component';
import { UserCardComponent } from './user-card/user-card.component';
import { RegistrationComponent } from './registration/registration.component';
import { DbInfoService } from './db-info.service';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    CardsComponent,
    LoginComponent,
    SingleCardComponent,
    CreateCardComponent,
    UserCardComponent,
    RegistrationComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    //1 как я понял тут страницы где path адресс, component - элемент который отвечает за проработку адресса
    RouterModule.forRoot([
      { path: '', component: CardsComponent, pathMatch: 'full' },
      { path: 'cards', component: CardsComponent },
      { path: 'cards/:id', component: SingleCardComponent },
      { path: 'login', component: LoginComponent },
      { path: 'create', component: CreateCardComponent },
      { path: 'reg', component: RegistrationComponent },
      { path: 'my', component: UserCardComponent },
      { path: '**', component: NotFoundComponent },
    ])
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: InterceptorService,
    multi: true,
  },
  DbInfoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
