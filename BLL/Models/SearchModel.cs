﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    /// <summary>
    /// Represents model for searching 
    /// </summary>
    public class SearchModel
    {
        public string UserName { get; set; }
        public DateTime PublishedAfter { get; set; }
        public DateTime PublishedBefore { get; set; }
        public bool? Publicity { get; set; }
        public string SphereName { get; set; }
        public bool Active { get; set; } = true;
        public ICollection<string> Tags { get; set; }
    }
}
