﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    /// <summary>
    /// Represents the card
    /// </summary>
    /// <param name="UserId">id of the user card owner</param>
    /// <param name="User">the user card owner</param>
    /// <param name="Published">DateTime when card was published</param>
    /// <param name="Publicity">true if the card is public, false if the card is private</param>
    /// <param name="Tags">tags of the card</param>
    /// <param name="Sphere">sphere of the card</param>
    /// <param name="Status">status of the card</param>
    /// <param name="Text">text of the card</param>
    public class Card : BaseEntity
    {
        public int UserId { get; set; }
        public IdentityUser User { get; set; }
        public DateTime Published { get; set; }
        public bool Publicity { get; set; }
        public ICollection<CardTag> Tags { get; set; }
        public Sphere Sphere { get; set; }
        public Status Status { get; set; }
        public string Text { get; set; }
    }
}
