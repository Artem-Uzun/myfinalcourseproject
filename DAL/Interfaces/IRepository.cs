﻿using Data.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    /// <summary>
    /// Represents the interface of repository to work with TEntity class
    /// </summary>
    /// <typeparam name="TEntity">Entity to work with</typeparam>
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        IQueryable<TEntity> FindAll();

        Task<TEntity> GetByIdAsync(int id);
        Task AddAsync(TEntity entity);

        void Update(TEntity entity);

        void Delete(TEntity entity);

        void DeleteById(int id);
    }
}
