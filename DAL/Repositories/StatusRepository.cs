﻿using Data.Entities;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    /// <summary>
    /// implements IStatusRepository
    /// </summary>
    public class StatusRepository : IStatusRepository
    {
        private readonly CardFileDbContext db;
        public StatusRepository(CardFileDbContext context)
        {
            this.db = context;
        }

        public async Task AddAsync(Status entity)
        {
            await db.Statuses.AddAsync(entity);
        }

        public void Delete(Status entity)
        {
            db.Statuses.Remove(entity);
        }

        public void DeleteById(int id)
        {
            var toRemove = db.Statuses.Find(id);
            db.Statuses.Remove(toRemove);
        }
        public IQueryable<Status> FindAll()
        {
            return db.Statuses;
        }
        public async Task<Status> GetByIdAsync(int id)
        {
            return await db.Statuses.FindAsync(id);
        }
        public void Update(Status entity)
        {
            var statusUpdate = db.Statuses.First(x => x.Id == entity.Id);
            if (statusUpdate != null)
            {
                statusUpdate.Text = entity.Text;
                statusUpdate.Active = entity.Active;
                statusUpdate.AdminBlocked = entity.AdminBlocked;
            }
        }
    }
}
