﻿using Data.Entities;
using Data.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    /// <summary>
    /// The class is used to initialize db with start data
    /// </summary>
    public class DataInitializer
    {
        static UserManager<IdentityUser> userManager;
        static RoleManager<IdentityRole> roleManager;
        static ICardRepository cardRepository;
        static ISphereRepository sphereRepository;
        static IStatusRepository statusRepository;
        static ITagRepository tagRepository;
        /// <summary>
        /// The function is used to initialize db with start data
        /// </summary>
        /// <param name="u">Unit of work to work with db</param>
        /// <exception>if there are any problems with adding data to the database</exception>
        public static async Task InitializeAsync(IUnitOfWork u)
        {
            userManager = u.UserManager;
            roleManager = u.RoleManager;
            cardRepository = u.CardRepository;
            sphereRepository = u.SphereRepository;
            statusRepository = u.StatusRepository;
            tagRepository = u.TagRepository;
            var transaction = u.GetTransaction();
            try
            {
                await InitializeSpheres();
                await InitializeTags();

                string password = "Pa$$w0rd";
                if (await roleManager.FindByNameAsync("admin") == null)
                {
                    await roleManager.CreateAsync(new IdentityRole("admin"));
                }
                if (await roleManager.FindByNameAsync("user") == null)
                {
                    await roleManager.CreateAsync(new IdentityRole("user"));
                }
                if (await userManager.FindByNameAsync("admin") == null)
                {
                    IdentityUser admin = new IdentityUser { UserName = "admin" };
                    IdentityResult result = await userManager.CreateAsync(admin, password);
                    if (result.Succeeded)
                    {
                        await userManager.AddToRoleAsync(admin, "admin");
                    }
                }
                if (await userManager.FindByNameAsync("artem") == null)
                {
                    IdentityUser artem = new IdentityUser { UserName = "artem" };
                    IdentityResult result = await userManager.CreateAsync(artem, "!%0LZwK9CRqy");
                    if (result.Succeeded)
                    {
                        await userManager.AddToRoleAsync(artem, "user");
                        await InitializeCards(await userManager.FindByNameAsync("artem"), await userManager.FindByNameAsync("admin"));
                    }
                }

                await u.SaveAsync();

                await transaction.CommitAsync();
            }
            catch
            {
                await transaction.RollbackAsync();
                throw;
            }
        }
        /// <summary>
        /// Function to initialize user cards
        /// </summary>
        /// <param name="user">user</param>
        /// <param name="admin">admin</param>
        public static async Task InitializeCards(IdentityUser user, IdentityUser admin)
        {
            var taglist = new List<Tag>();
            taglist.Add(tagRepository.FindByName("nature"));
            var natureTag = tagRepository.FindByName("nature");
            var card1Status = new Status
            {
                Active = true
            };
            await statusRepository.AddAsync(card1Status);
            Card card1 = new Card
            {
                Publicity = true,
                Published = DateTime.Parse("26.05.2021"),
                User = user,
                Sphere = sphereRepository.FindByName("art"),
                Status = card1Status,
                Text = @"One of the most remarkable artists to ever live, Henry Matisse once said: “An artist must possess Nature. He must identify himself with her rhythm, by efforts that will prepare the mastery which will later enable him to express himself in his own language.”

For as long as there has been art,
                artists have been enthused by nature.Apart from providing endless inspiration,
                many of the mediums that artists use to create their masterpieces such as wood,
                charcoal,
                clay,
                graphite,
                and water are all products from nature. "
            };
            await cardRepository.AddAsync(card1);
            var card1tag = new CardTag
            {
                Card = card1,
                Tag = natureTag
            };
            await tagRepository.AddCardTagAsync(card1tag);

            var card2Status = new Status
            {
                Active = true
            };
            await statusRepository.AddAsync(card2Status);
            Card card2 = new Card
            {
                Publicity = false,
                Published = DateTime.Parse("26.05.2021"),
                User = user,
                Status = card2Status,
                Text = @"Nature, in the broadest sense, is equivalent to the natural world, physical universe, material world or material universe. “Nature” refers to the phenomena of the physical world, and also to life in general"
            };
            await cardRepository.AddAsync(card2);

            var card3Status = new Status
            {
                Active = false,
                AdminBlocked = admin,
                Text = "violation of site rules"
            };
            await statusRepository.AddAsync(card3Status);
            Card card3 = new Card
            {
                Publicity = true,
                Published = DateTime.Parse("26.05.2021"),
                User = user,
                Sphere = sphereRepository.FindByName("art"),
                Status = card3Status,
                Text = @"One of the most remarkable artists to ever live, Henry Matisse once said: “An artist must possess Nature. He must identify himself with her rhythm, by efforts that will prepare the mastery which will later enable him to express himself in his own language.”

For as long as there has been art,
                artists have been enthused by nature.Apart from providing endless inspiration,
                many of the mediums that artists use to create their masterpieces such as wood,
                charcoal,
                clay,
                graphite,
                and water are all products from nature. "
            };
            await cardRepository.AddAsync(card3);

            var card3tag = new CardTag
            {
                Card = card3,
                Tag = natureTag
            };
            await tagRepository.AddCardTagAsync(card3tag);
        }
        /// <summary>
        /// Function to initialize spheres
        /// </summary>
        public static async Task InitializeSpheres()
        {
            if (sphereRepository.FindByName("science") == null)
            {
                await sphereRepository.AddAsync(new Sphere { Name = "science" });
            }
            if (sphereRepository.FindByName("space") == null)
            {
                await sphereRepository.AddAsync(new Sphere { Name = "space" });
            }
            if (sphereRepository.FindByName("sport") == null)
            {
                await sphereRepository.AddAsync(new Sphere { Name = "sport" });
            }
            if (sphereRepository.FindByName("art") == null)
            {
                await sphereRepository.AddAsync(new Sphere { Name = "art" });
            }
        }
        /// <summary>
        /// function to initialize tags
        /// </summary>
        public static async Task InitializeTags()
        {
            if (tagRepository.FindByName("spacex") == null)
            {
                await tagRepository.AddAsync(new Tag { TagName = "spacex" });
            }
            if (tagRepository.FindByName("nature") == null)
            {
                await tagRepository.AddAsync(new Tag { TagName = "nature" });
            }
            if (tagRepository.FindByName("fitlife") == null)
            {
                await tagRepository.AddAsync(new Tag { TagName = "fitlife" });
            }
            if (tagRepository.FindByName("trainhard") == null)
            {
                await tagRepository.AddAsync(new Tag { TagName = "trainhard" });
            }
            if (tagRepository.FindByName("holiday") == null)
            {
                await tagRepository.AddAsync(new Tag { TagName = "holiday" });
            }
        }
    }
}
