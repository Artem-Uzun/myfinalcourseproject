import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BlockedCardModel, CardModel } from '../types';

@Component({
  selector: 'app-single-card',
  templateUrl: './single-card.component.html',
  styleUrls: ['./single-card.component.css']
})
export class SingleCardComponent implements OnInit {
  cardId: number;
  response: CardModel;
  errorResponse: string;
  username: string;
  role: string;
  disableButton: boolean;
  baseUrl: string;
  banButtonPressed: boolean;
  disableBanButton: boolean;
  cardIsBanned: boolean;
  banForm: FormGroup;
  cardUserName: string;

  constructor(private formBuilder: FormBuilder, private http: HttpClient, @Inject('BASE_URL') baseUrl: string,
              route: ActivatedRoute, private router: Router) { 
    const routeParams = route.snapshot.paramMap;
    this.cardId = Number(routeParams.get('id'));
    http.get<CardModel>(baseUrl + 'api/cards/' + this.cardId).subscribe(result => {
      this.response = result;
      this.cardUserName = result.userName;
      this.disableButton = !result.active;
      this.cardIsBanned = !result.active;
    }, error =>{
       this.errorResponse = error.error;
    });
    this.baseUrl = baseUrl;
  }

  ngOnInit() {
    this.disableBanButton = false;
    this.banButtonPressed = false;
    this.username = localStorage.getItem('userName');
    this.role = localStorage.getItem('role');
    this.banForm = this.formBuilder.group({
      reason: '',
    });
  }
  submitDelete(): void {
    this.disableButton = true;
    this.http.delete<number>(this.baseUrl + 'api/cards/' + this.cardId)
    .subscribe(result => {
      this.router.navigate(['cards']);
    }, error => {
      this.errorResponse = error.error; 
      this.disableButton = false;
    });
  }
  goToEdit(){
    this.router.navigate(['/create'], {state: {model: this.response}});
  }
  showBanForm(): void {
    this.banButtonPressed = true;
    this.disableButton = true;
  }
  stopShowBanForm(): void {
    this.banButtonPressed = false;
    this.disableButton = false;
  }
  submitBan(): void {
    this.disableBanButton = true;
    let ban = this.banForm.getRawValue();
    let banCard = {
      id: this.cardId,
      reason: ban.reason,
    };
    this.http.post<BlockedCardModel>(this.baseUrl + 'api/cards/ban', banCard)
    .subscribe(result => {
     this.router.navigate(['cards']);
    }, error => {
      this.errorResponse = error.error;
      this.disableBanButton = false;
    });
  }
  submitUnBan(): void {
    this.cardIsBanned = false;
    let id =  this.cardId;
    this.http.post(this.baseUrl + 'api/cards/unban', id)
    .subscribe(result => {
     this.router.navigate(['cards']);
    }, error => {
      this.errorResponse = error.error;
      this.cardIsBanned = true;
    });
  }
}
