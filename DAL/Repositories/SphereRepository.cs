﻿using Data.Entities;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    /// <summary>
    /// implements ISphereRepository
    /// </summary>
    public class SphereRepository : ISphereRepository
    {
        private readonly CardFileDbContext db;
        public SphereRepository(CardFileDbContext context)
        {
            this.db = context;
        }

        public async Task AddAsync(Sphere entity)
        {
            await db.Spheres.AddAsync(entity);
        }

        public void Delete(Sphere entity)
        {
            db.Spheres.Remove(entity);
        }

        public void DeleteById(int id)
        {
            var toRemove = db.Spheres.Find(id);
            db.Spheres.Remove(toRemove);
        }

        public IQueryable<Sphere> FindAll()
        {
            return db.Spheres;
        }
        public Sphere FindByName(string name)
        {
            return db.Spheres.FirstOrDefault(x=>x.Name == name);
        }

        public async Task<Sphere> GetByIdAsync(int id)
        {
            return await db.Spheres.FindAsync(id);
        }
        public void Update(Sphere entity)
        {
            var sphere = db.Spheres.First(x => x.Id == entity.Id);
            if (sphere != null)
            {
                sphere.Name = entity.Name;
            }
        }
    }
}
