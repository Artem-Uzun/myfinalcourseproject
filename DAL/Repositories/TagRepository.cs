﻿using Data.Entities;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    /// <summary>
    /// implements ITagRepository
    /// </summary>
    public class TagRepository :ITagRepository
    {
        private readonly CardFileDbContext db;
        public TagRepository(CardFileDbContext context)
        {
            this.db = context;
        }

        public async Task AddAsync(Tag entity)
        {
            await db.Tags.AddAsync(entity);
        }
        public async Task AddCardTagAsync(CardTag entity)
        {
            await db.CardTags.AddAsync(entity);
        }
        public void DeleteCardTag(int cardId, int tagId)
        {
            var toRemove = db.CardTags.First(x => x.CardId == cardId && x.TagId == tagId);
            db.CardTags.Remove(toRemove);
        }
        public IEnumerable<CardTag> GetCardTags()
        {
            return db.CardTags;
        }
        public void Delete(Tag entity)
        {
            db.Tags.Remove(entity);
        }

        public void DeleteById(int id)
        {
            var toRemove = db.Tags.Find(id);
            db.Tags.Remove(toRemove);
        }
        public Tag FindByName(string name)
        {
            return db.Tags.FirstOrDefault(x => x.TagName == name);
        }

        public IQueryable<Tag> FindAll()
        {
            return db.Tags;
        }

        public async Task<Tag> GetByIdAsync(int id)
        {
            return await db.Tags.FindAsync(id);
        }
        public void Update(Tag entity)
        {
            var tag = db.Tags.First(x => x.Id == entity.Id);
            if (tag != null)
            {
                tag.TagName = entity.TagName;
            }
        }
    }
}
