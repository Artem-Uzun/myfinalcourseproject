import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ResponseModel, UserModel } from '../types';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  baseUrl:string;
  response: ResponseModel;
  disableButton: boolean;
  formErrors: string;

  constructor(private formBuilder:FormBuilder, private http: HttpClient,
             @Inject('BASE_URL') baseUrl: string, private router: Router) { 
    this.baseUrl = baseUrl;
  }

  ngOnInit() {
    this.disableButton = false;
    this.form = this.formBuilder.group({
      username: '',
      password: ''
    });
  }
  submit(): void {
    this.disableButton = true;
    this.http.post<ResponseModel>(this.baseUrl + 'api/account/login', this.form.getRawValue())
    .subscribe(result => {
      localStorage.setItem('token',result.message);
      localStorage.setItem('role', result.userRole);
      localStorage.setItem('userName', result.userName);
      this.response = result;
      this.response.message = 'Successfully logined';
      this.router.navigate(['/my']);
      setTimeout(()=>{ window.location.reload() }, 1000)
    }, error => {
      this.formErrors = error.error.errors;
      this.response = error.error;
      this.disableButton = false;
    });
  }
}
