import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {
  token:boolean;
  username: string;

  constructor(private router: Router){  }

  ngOnInit(): void {
    this.token = (localStorage.getItem('token')!==null);
    this.username = localStorage.getItem('userName');
  }
  isExpanded = false;

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }
  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    localStorage.removeItem('userName');
    this.router.navigate(['/cards']);
    setTimeout(()=>{ window.location.reload() }, 1000)
  }
}
