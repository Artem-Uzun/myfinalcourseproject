﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    /// <summary>
    /// Represents the Tag for specifying card info
    /// </summary>
    /// <param name="Name">Name of the sphere</param>
    public class Sphere : BaseEntity
    {
        public string Name { get; set; }
    }
}
